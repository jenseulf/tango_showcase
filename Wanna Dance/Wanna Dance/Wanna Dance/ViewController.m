//
//  ViewController.m
//  Wanna Dance
//
//  Created by Jensen, Ulf on 11/25/15.
//  Copyright © 2015 adidas. All rights reserved.
//

#import "ViewController.h"
#import "WebService.h"

@interface ViewController () <UITextFieldDelegate>

@end

@implementation ViewController {

    __weak  IBOutlet UITextField *_userId;
    __weak  IBOutlet UITextField *_activityType;
    __weak  IBOutlet UITextField *_name;
    __weak IBOutlet UITextField *_distance;
   
    __weak IBOutlet UIButton *_startDate;
    __weak IBOutlet UIButton *_endDate;
    

    __weak IBOutlet UILabel *_workoutId;
    __weak IBOutlet UILabel *_runScore;

    __weak IBOutlet UIButton *_postWorkout;
    __weak IBOutlet UIButton *_getRunScore;
    
    __weak IBOutlet UIBarButtonItem *_startDateDone;
    __weak IBOutlet UIView *_startDateView;
    __weak IBOutlet UIDatePicker *_startDatePicker;

    __weak IBOutlet UIBarButtonItem *_endDateDone;
    __weak IBOutlet UIDatePicker *_endDatePicker;
    __weak IBOutlet UIView *_endDateView;
    
    //Holds the current active test field, needed to hide keyboard
    __weak UITextField *_activeTextField;
    
    //Connection to the server/backend, does the URL calls
    WebService *_webService;
}

//Called when get RunScore button clicked
- (IBAction)onGetRunScore:(id)sender {
    
    [_webService getRunScore:_workoutId.text];
}

//Called when post workout button clicked
- (IBAction)onPostWorkout:(id)sender {

    //Creates the paylod from input and static data
    NSArray *feed = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:2],[NSNumber numberWithInt:3], nil];
    NSDictionary *timeSerie = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"test",@"feed_type",
                              _startDate.titleLabel.text, @"start_date",
                              _endDate.titleLabel.text, @"end_date",
                              feed, @"feed",
                              nil];
    NSArray *timeSeries = [[NSArray alloc] initWithObjects:timeSerie, nil];
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                _workoutId.text, @"workout_id",
                                _userId.text, @"user_id",
                                _activityType.text, @"activity_type",
                                _name.text, @"name",
                                _distance.text, @"distance",
                                _startDate.titleLabel.text, @"start_date",
                                _endDate.titleLabel.text, @"end_date",
                                timeSeries,@"time_series",
                                nil];
    
    [_webService postWorkout:dictionary];
}

//Called when starting to edit input text
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [_startDateView setHidden:YES];
    [_endDateView setHidden:YES];
    
    _activeTextField = textField;
}

//Called when return is hit on keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_startDateView setHidden:YES];
    [_endDateView setHidden:YES];

    _activeTextField = nil;
    [textField resignFirstResponder];//hide keyboard

    return YES;
}

//Called when start date field button is clicked
- (IBAction)onStartDate:(id)sender {
    [_startDateView setHidden:NO];
    [_endDateView setHidden:YES];
    [_activeTextField resignFirstResponder];//hide keyboard
    
}

//Called when start date picker done button is clicked
- (IBAction)onStartDateDone:(id)sender {
    [_startDateView setHidden:YES];
    [_endDateView setHidden:YES];
    [_activeTextField resignFirstResponder];//hide keyboard
    
    _startDate.titleLabel.text = [self getStringFromDate:_startDatePicker.date];
}

//Called when end date field button is clicked
- (IBAction)onEndDate:(id)sender {
    [_startDateView setHidden:YES];
    [_endDateView setHidden:NO];
    [_activeTextField resignFirstResponder];//hide keyboard
}

//Called when end date picker done button is clicked
- (IBAction)onEndDateDone:(id)sender {
    [_startDateView setHidden:YES];
    [_endDateView setHidden:YES];
    [_activeTextField resignFirstResponder];//hide keyboard
    
    _endDate.titleLabel.text = [self getStringFromDate:_endDatePicker.date];
}

//Callback from WebService class when post workout was successful
-(void) receivePostWorkoutSuccess:(NSNotification *) notification{

    NSDictionary *dictionary = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{//this is needed as the user interface is changed from a different thread
        _workoutId.text = [self getWorkoutIdFromPayload:dictionary];
    });
}

//Callback from WebService class when post workout was erroneous
-(void) receivePostWorkoutError:(NSNotification *) notification{

    dispatch_async(dispatch_get_main_queue(), ^{//this is needed as the user interface is changed from a different thread
        _workoutId.text = @"-1";//Change this when an error should be displayed differently
    });
    
}

//Callback from WebService class when get runscore was successful
-(void) receiveGetRunscoreSuccess:(NSNotification *) notification{

    NSDictionary *dictionary = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{//this is needed as the user interface is changed from a different thread
        NSString *runScore = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"value"]];
        _runScore.text = runScore;
    });
}

//Callback from WebService class when get runscore was erroneous
-(void) receiveGetRunscoreError:(NSNotification *) notification{

    dispatch_async(dispatch_get_main_queue(), ^{//this is needed as the user interface is changed from a different thread
        _runScore.text = @"-1";//Change this when an error should be displayed differently
    });

}

//Called when user interface is initialized
- (void)viewDidLoad {
    [super viewDidLoad];

    //Declares a web service instance
    _webService = [[WebService alloc] init];
    
    //Hide date picker on default
    [_startDateView setHidden:YES];
    [_endDateView setHidden:YES];
    
    //Add notification observers to be able to receive notifications from web server
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivePostWorkoutSuccess:) name:@"PostWorkoutSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivePostWorkoutError:) name:@"PostWorkoutError" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGetRunscoreSuccess:) name:@"GetRunscoreSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGetRunscoreError:) name:@"GetRunscoreError" object:nil];
    
}

//No idea
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Helper function to create the right date string format for payload
-(NSString *)getStringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSString *dateString = [dateFormatter stringFromDate:date];

    return dateString;
}

//Helper function to get the workout id from payload
- (NSString *)getWorkoutIdFromPayload:(NSDictionary *)dictionary{
    NSString *value = [dictionary objectForKey:@"url"];
    NSArray *splitString = [value componentsSeparatedByString: @"/"];
    NSString *workoutId = [splitString objectAtIndex: 4];
    
    return workoutId;
}

@end