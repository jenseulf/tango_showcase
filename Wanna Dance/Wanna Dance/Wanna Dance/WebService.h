//
//  WebService.h
//  Wanna Dance
//
//  Created by Jensen, Ulf on 11/25/15.
//  Copyright © 2015 adidas. All rights reserved.
//

#ifndef WebService_h
#define WebService_h

@interface WebService : NSObject

-(void)postWorkout:(NSDictionary *)dictionary;
-(void)getRunScore:(NSString *)string;

@end


#endif /* WebService_h */
