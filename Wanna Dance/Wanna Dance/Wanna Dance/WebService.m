//
//  WebService.m
//  Wanna Dance
//
//  Created by Jensen, Ulf on 11/25/15.
//  Copyright © 2015 adidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebService.h"

@implementation WebService

//Function to do the post workout call.
//Notifies the user interface with a notification about result (success or error)
//Input: A dictionary structure that represents the payload
-(void)postWorkout:(NSDictionary *)dictionary{
 
    NSURL * url = [NSURL URLWithString:@"http://dsopdev005.cloudapp.net:9003/core/me/workouts"];
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postdata];
    
    NSLog(@"Payload: %@",[[NSString alloc] initWithData:postdata encoding: NSUTF8StringEncoding]);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSLog(@"Post workout response received.");
            if(error == nil) {
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSLog(@"Result: %@",dictionary);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostWorkoutSuccess" object:nil userInfo:dictionary];

            } else {
                NSLog(@"Error: %@",error);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostWorkoutError" object:nil];
            }
    }] resume];
}

//Function to do the get runscore call.
//Notifies the user interface with a notification about result (success or error)
//Input: A string that represents the workout id
-(void)getRunScore:(NSString *)workoutIdString{
    
    NSString *urlString = [@"http://dsopdev005.cloudapp.net:9003/plugins/runscore/" stringByAppendingString:workoutIdString];
    NSLog(@"Get RunScore url: %@", urlString);
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Get RunScore response received.");
        if(error == nil){
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"Result: %@",dictionary);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRunscoreSuccess" object:nil userInfo:dictionary];
            
        } else {
            NSLog(@"Error: %@",error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRunscoreError" object:nil];
        }
    }] resume];
}



@end
